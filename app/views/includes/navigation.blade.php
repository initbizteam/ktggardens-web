<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#topWrap">
                <span class="glyphicon glyphicon-tree-deciduous"></span>
                KTG<span class="title">gardens.com</span>
            </a>
        </div>
        <div class="collapse navbar-collapse appiNav">
            <ul class="nav navbar-nav">
                <li><a href="{{URL::to('/')}}">Home</a></li>
                <li><a href="{{URL::to('/offer')}}">Offer</a></li>
                <li><a href="{{URL::to('/projects')}}">Projects</a></li>
                <li><a href="{{URL::to('/companies')}}">Companies</a></li>
                <li><a href="{{URL::to('/contact')}}">Contact</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>