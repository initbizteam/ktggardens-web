<footer style="position:fixed; bottom: 0; left: 0; width: 100%">
    <div class="container">
        <div class="row" style="padding-top: 5px">
            <div class="col-xs-6 text-left">
                <p>Copyright &copy; 2014 KTGgardens. All Rights Reserved</p>
            </div>
            <div class="col-xs-6 text-right">
                <p class="social">
                    <a href="https://www.facebook.com/KTGGardens" target="_blank">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="https://uk.linkedin.com/pub/tomasz-grabka/94/181/867" target="_blank">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="#" target="_blank">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                </p>
            </div>
        </div>
    </div>
</footer>