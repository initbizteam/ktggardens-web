@extends('layouts.default')
@section('content')
       <div class="content">
            <div id="contactWrap">
                <div class="overlay">
                    <div class="container">
                        <div class="col-md-4">
                            <div class="panel panel-success">
                              <div class="panel-heading">
                                <h3 class="panel-title">Lawns</h3>
                              </div>
                              <div class="panel-body">
                                <ul>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> preparing ground for lawn</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> establishing lawn from seeds</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> establishing lawn from rolls</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> cutting lawn</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> aeration</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> verticulation</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> feritilizing</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> weeding</p></li>
                                </ul>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                              <div class="panel-heading">
                                <h3 class="panel-title">Ground works</h3>
                              </div>
                              <div class="panel-body">
                                <ul>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> Chemical preparation of the ground</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> Mechanical preparation of the ground</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> landscaping</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> modification of land for the project</p></li>
                                </ul>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                              <div class="panel-heading">
                                <h3 class="panel-title">Water in the garden</h3>
                              </div>
                              <div class="panel-body">
                                <ul>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> ponds</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> cascades</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> stream and watercourses</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> irrigation systems</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> cleaning and repairs ponds</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> flora and fauna of the ponds</p></li>
                                </ul>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-md-4">
                            <div class="panel panel-success">
                              <div class="panel-heading">
                                <h3 class="panel-title">Services for communities, landlords and housing agencies</h3>
                              </div>
                              <div class="panel-body">
                                <ul>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> Spring garden maintenance</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> Garden cleaning works</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> Comprehensive maintenance of green areas and gardens - all season</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> Autumn comprehensive work, cleaning, securing for the winter</p></li>
                                    <li><p><span class="glyphicon glyphicon-arrow-right"></span> Snow clearing</p></li>
                                </ul>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                              <div class="panel-heading">
                                <h3 class="panel-title">Garden planning</h3>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>

@stop