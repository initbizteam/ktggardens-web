@extends('layouts.default')
@section('content')
<header style="display: block; margin-top: 20px;">
    <div id="caro">
        <div id="featureWrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 text-left feature">
                        <span class="fa-stack fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-users fa-stack-1x fa-inverse"></i>
                        </span>
                        <div id="about" style="background-color: rgba(0,128,0,0.9); display: none">
                            <hr/>
                            <p>
                            Your local friendly gardening and landscaping company. Create to help people with garden jobs and duties.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-left feature">
                        <span class="fa-stack fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-truck fa-stack-1x fa-inverse"></i>
                        </span>
                        <div id="offer" style="background-color: rgba(0,128,0,0.9); display: none">
                        <hr/>
                            <p>grass cutting</p>
                            <p>pruning</p>
                            <p>lawn care</p>
                            <p>hedge trimming</p>
                            <p>general garden maintenance</p>
                            <p>garden planning</p>
                            <p>planting</p>
                            <p>weeding</p>
                            <p>all sorts of garden jobs, feel free to ask</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-left feature">
                        <span class="fa-stack fa-3x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-globe fa-stack-1x fa-inverse"></i>
                        </span>
                        <div id="social" style="background-color: rgba(0,128,0,0.9); display: none">
                            <hr/>
                            <a href="http://www.facebook.com/KTGGardens/">facebook</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /featureWrap -->
    </div>
</header>


@stop