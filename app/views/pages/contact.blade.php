@extends('layouts.default')
@section('content')
   <div class="content">
        <div id="contactWrap">
            <div class="overlay">
                <div class="container">
                    <div class="col-sm-5 col-sm-offset-1">
                    {{ Form:: open(array('url' => 'submit')) }}
                        <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Name*</div>
                                    {{ Form:: text ('name', '',array('class' => 'form-control', 'required' => 'required')) }}
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">E-mail*</div>
                                  {{ Form:: email ('email', '', array('class' => 'form-control', 'required' => 'required')) }}
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                {{ Form:: label ('message', 'Message*', array('for' => 'message') )}}
                                {{ Form:: textarea ('message', '', array('class' => 'form-control', 'rows' => '3', 'cols' => '25', 'required' => 'required'))}}
                            </div>
                        </div>
                        <div class="alert alert-info info" style="display: none">
                            <ul></ul>
                        </div>
                       <div class="row">
                           <div class="col-sm-1">
                                {{ Form::submit('Send', array('class' => 'btn btn-primary btn-lg')) }}
                           </div>
                           <fiv class="col-sm-1 col-sm-offset-1">
                                {{ Form::reset('Clear', array('class' => 'btn btn-primary btn-lg')) }}
                           </fiv>
                       </div>
                    {{ Form::close() }}
                    </div>
                    <div class="col-sm-6" style="border-left: 1px solid #008000">
                        <div class="row">
                            <div class="col-sm-12">
                                <p><b>KTG Gardens</b></p>
                                <p><b>Tomasz Grabka</b></p>
                                <p><b>Telephone:</b></p>
                                <p><b>E-mail: </b></p>
                            </div>
                        </div>
                        <div id="map"></div>

                    </div>
                </div>
            </div>
        </div> <!-- /contactWrap -->
   </div>
<script>
       $(document).ready(function() {
        var alert = $('.info');
            $('form').submit(function(e) {
                e.preventDefault();

                var formData = {
                    'name' : $('input[name=name]').val(),
                    'email' : $('input[name=email]').val(),
                    'message' : $('#message').val()
                };

                $.ajax({
                    url: 'submit',
                    method: 'post',
                    data: formData,
                    cache: false,
                    dataType: 'json',
                    success: function(data) {

                        alert.hide().find('ul').empty();

                        if(!data.success) {
                            $.each(data.error, function(index, error) {
                                alert.find('ul').append('<li>'+ error + ' </li>');
                            });
                            alert.slideDown();
                        } else {
                            alert.find('ul').append('<li> Thank you for your message!</li>');
                            alert.slideDown();
                        }
                    },
                    error: function(data){
                    console.log(data.responseText);
                     $.each(data.error, function(index, error) {
                                                    alert.find('ul').append('<li>'+ error + ' </li>');
                                                });
                    }
                });
            });
       });
   </script>
@stop