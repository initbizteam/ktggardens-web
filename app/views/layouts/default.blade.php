<!DOCTYPE html>
<html>
<head>
	<title>KTGgardens.com</title>
	<meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, maximum-scale=1.0">
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/custom.css') }}
    {{ HTML::style('css/flexslider.css') }}
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    {{ HTML::script('js/jquery.min.js') }}
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&extension=.js&output=embed"></script>
    {{ HTML::script('js/map.js') }}
</head>
<body>
	@include('includes.navigation')
	@yield('content')
	@include('includes.footer')


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/flexslider.js') }}
    {{--<script>--}}
        {{--$(document).ready(function() {--}}

         {{--$('#caro').carousel({--}}
              {{--interval : 100000,--}}
              {{--pause: true--}}
         {{--});--}}

        {{--});--}}
    {{--</script>--}}
</body>
</html>