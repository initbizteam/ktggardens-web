<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('pages.home');
});
Route::get('/contact', function()
{
	return View::make('pages.contact');
});
Route::get('/offer', function()
{
    return View::make('pages.offer');
});
Route::get('/companies', function()
{
    return View::make('pages.companies');
});
Route::post('submit', function()  {
	$data = Input::all();

    $rules = array(
        'name' => 'required|alpha',
        'email' => 'required|email',
        'message' => 'required|min:5'
    );

    $validator = Validator::make($data, $rules);

    if($validator ->passes()) {
        Mail::send('emails.contactmail',$data, function($message) use ($data) {
            $message->from('biuro@init.biz', 'Formularz www');
            $message->to('biuro@init.biz', 'Biuro')->subject($data['name']);
            $message->replyTo($data['email']);
        });
        return Response::json(['success' => true], 200);
    } else {
        echo "blad...";
        return Response::json(array(
            'success' => false,
            'error' => $validator->errors()->toArray()
        ));
    }
});
Route::get('/projects', function()
{
	return View::make('pages.projects');
});
