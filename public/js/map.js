
$(document).ready(function(){/* google maps -----------------------------------------------------*/
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {

        /* position */
        var latlng = new google.maps.LatLng(50.7235373, -2.0170902);

        var mapOptions = {
            center: latlng,
            scrollWheel: false,
            zoom: 16
        };

        var marker = new google.maps.Marker({
            position: latlng,
            url: '/',
            title: "Marker at " + latlng,
            animation: google.maps.Animation.DROP
        });


        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        marker.setMap(map);


    };
    /* end google maps -----------------------------------------------------*/
});